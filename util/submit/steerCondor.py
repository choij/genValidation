import os

def steer(submitLines, datasetname, runpointDir):

    os.system("cp {0} {1}".format(os.path.join("config", "submit", "condor.jds"), os.path.join(runpointDir, "condor.jds")))
    os.system("sed -i 's|\[datasetname\]|{0}|g' {1}".format(datasetname, os.path.join(runpointDir, "condor.jds")))

    submitLines += "cd {0}\n".format(os.path.join(runpointDir))
    submitLines += "condor_submit condor.jds\n"
    submitLines += "cd -\n"

    return submitLines
