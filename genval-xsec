#!/usr/bin/python3

"""
=== MG validation team ===
Authors: Jin Choi (choij@cern.ch)
Maintainers:
"""

import os, sys
import argparse
from math import pow, sqrt

# parse argument first
parser = argparse.ArgumentParser("Generator validation scripts")
parser.add_argument("-d", "--datasetname", action="store", dest="datasetname", \
                    default=None, required=True, type=str, help="dataset name")
parser.add_argument("-j", "--njets", action="store", dest="njets", \
                    default=None, required=True, type=int, help="number of jet bins")
args = parser.parse_args()
dataset = args.datasetname
njets = args.njets

if not os.path.exists(dataset):
    print(f"[genval-xsec] No dataset {dataset}")
    exit()
else:
    print(f"[genval-xsec] Estimating x-sec for {dataset}...")

def store_runpoints(dataset_path):
    sub_dirs = os.listdir(dataset_path)
    runpoints = list(filter(lambda d: "runpoint" in d, sub_dirs))
    return runpoints

def store_info(dataset_path):
    """Read result.log from each run point"""
    """And store the xsec & matching information"""
    runpoints = store_runpoints(dataset_path)
    p = f"{dataset}/{runpoints[0]}/result.log"
    xsecs = dict()
    nevts = dict()
    for i in range(njets+1):
        xsecs[f"{i}j"] = []
        nevts[f"{i}j"] = []
    xsecs["total"] = []
    nevts["total"] = []
    for runpoint in runpoints:
        path = f"{dataset}/{runpoint}/result.log"
        with open(path, 'r') as f:
            for l in f.readlines():
                if not "+/-" in l: continue
                else:
                    try:
                        if int(l[0]) in range(njets+1):
                            key = f"{int(l[0])}j"
                            info_string = l.split("\t")
                            # xsec_before, xsec_match
                            xsecs[key].append((info_string[2], info_string[10]))
                            # npass_pos, npass_neg, ntotal_pos, ntotal_neg
                            nevts[key].append((int(info_string[5]), int(info_string[6]), int(info_string[8]), int(info_string[9])))
                    except:
                        if l[:5] == "Total":
                            info_string = l.split("\t")
                            xsecs["total"].append((info_string[2], info_string[10]))
                            nevts["total"].append((int(info_string[5]), int(info_string[6]), int(info_string[8]), int(info_string[9])))
    return xsecs, nevts

def eval_xsec_and_err(xsecs, errors):
    weighted_sum, error = 0., 0.
    for i in range(len(xsecs)):
        xsec, err = xsecs[i], errors[i]
        weighted_sum += xsec/pow(err, 2)
        error += 1./pow(err, 2)
    xsec = weighted_sum / error
    error = 1./sqrt(error)
    return (xsec, error)

def eval_matching_eff(xsecs, nevts):
    """Caculate matching efficiencies and xsec & err after matching"""
    """Based on https://github.com/cms-sw/cmssw/blob/master/GeneratorInterface/Core/plugins/GenXSecAnalyzer.cc#L411-L474"""
    print("[genval-xsec]\txsec_before[pb]\txsec_match[pb]\taccepted[%]")
    for key in xsecs.keys():
	    # get xsecs & err before matching first
        # why all the xsec errs same before matching?
        xsec = xsecs[key][0][0].split("+/-")
        xsec_before, xsec_before_err = float(xsec[0]), float(xsec[1])
        npass_pos = 0; npass_neg = 0; ntotal_pos = 0; ntotal_neg = 0
        for (np_pos, np_neg, nt_pos, nt_neg) in nevts[key]:
            npass_pos += np_pos
            npass_neg += np_neg
            ntotal_pos += nt_pos
            ntotal_neg += nt_neg
        npass = npass_pos-npass_neg; ntotal = ntotal_pos-ntotal_neg;
        fracAcc = npass/ntotal
        
        effp = npass_pos/ntotal_pos             if ntotal_pos != 0 else 0.
        effp_err2 = (1-effp)*effp/ntotal_pos    if ntotal_pos != 0 else 0.
        effn = npass_neg/ntotal_neg             if ntotal_neg != 0 else 0.
        effn_err2 = (1-effn)*effn/ntotal_neg    if ntotal_neg != 0 else 0.
        efferr2 = (pow(ntotal_pos, 2)*effp_err2+pow(ntotal_neg, 2)*effn_err2)/pow(ntotal, 2)
        delta2Veto = efferr2/pow(fracAcc, 2)
        delta2Sum = delta2Veto + pow(xsec_before_err, 2)/pow(xsec_before, 2)
        xsec_match, xsec_match_err = xsec_before*fracAcc, xsec_before*fracAcc*sqrt(delta2Sum)
        print(f"[{key}]\t{xsec_before:.2f} +/- {xsec_before_err:.2f}\t{xsec_match:.2f} +/- {xsec_match_err:.2f}\t{(fracAcc)*100:.2f}")

def main():
    xsecs, nevts = store_info(dataset)
    eval_matching_eff(xsecs, nevts)

if __name__ == "__main__":
    main()
