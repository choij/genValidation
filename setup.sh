#!/usr/bin/bash

echo "Setting up genValidation..."

SCRAM_ARCH="$(grep "SCRAM_ARCH=" config/run/cmsenv.dat)"
SCRAM_ARCH=${SCRAM_ARCH//\"/}
CMSSW="$(grep "CMSSW=" config/run/cmsenv.dat)"
CMSSW=${CMSSW//\"/}

echo "Setting up ${SCRAM_ARCH}, ${CMSSW} from config/run/cmsenv.dat..."

export $SCRAM_ARCH
export $CMSSW

cd /cvmfs/cms.cern.ch/${SCRAM_ARCH}/cms/cmssw/${CMSSW}/src/
eval `scramv1 runtime -sh`
cd ~-

if [ ! -f  "config/analysis/database/${CMSSW}.dat" ]
then
    echo "Setting up for the first time..."
    mkdir -p config/analysis/database
    echo "    Fetching Rivet's ATLAS analyses..."
    rivet --list-analyses "ATLAS_" >> config/analysis/database/${CMSSW}.dat
    echo "    Fetching Rivet's CMS analyses..."
    rivet --list-analyses "CMS_" >> config/analysis/database/${CMSSW}.dat
    echo "    Fetching Rivet's MC analyses..."
    rivet --list-analyses "MC_" >> config/analysis/database/${CMSSW}.dat
else
    echo "config/analysis/database/${CMSSW}.dat already exists..."
    echo "    remove config/analysis/database/${CMSSW}.dat if you want to start off fresh..."
fi

echo "Setup complete!"
